# Comments

## File: app.js

```js
var vals = [];
vals.push("A");
```

To save a line, you can just predefine `vals` like so.

```js
var vals = ["A"];
```

```js
drawCard: function() {
    // Your code here
},
```

In objects, definitions like this can be condensed to this.

```js
drawCard() {
    // Your code here
},
```

## File: index.html

```html
<link href="https://unpkg.com/animate.css@3.5.2/animate.min.css" rel="stylesheet"/>
```

While I know you did not use `animate.css` to run your animation, you can delete the `link` tag if it's not reuqired.

```html
<div id="card" :class="{'animated': animated}"> 
    <div id="top-val" :style="{color: color}">{{val}}</div>
    <div id="card-suit" :class="computedSuit"></div>
    <div id="bottom-val" :style="{color: color}">{{val}}</div>
</div>
```

It's better to use the `:style` directive when the values are a wide range like mouse position, brightness value, random color generator. When you only have two states, might as well use a `:class` derivative and write your own CSS rules. This is more scalable and allows you to separate Javascript logic and CSS styles.

## Design

Fonts, margins, and paddings were not followed. Be mindful of capitalization, shadows, letter-spacings and font-sizes. Look into importing Google Fonts.

## End notes

Great work on this project. You can improve your Javascript and Vue by reading more of the docs. 