var vals = [];
vals.push("A");
for (var i = 2; i <= 10; i++) {
  vals.push(i);
}
vals = vals.concat(["J", "Q", "K"]);
var suits = ["hearts", "diamonds", "clubs", "spades"];
new Vue({
  el: "#vue-app",
  data: {
    val: 2,
    suit: suits[2],
    color: "black",
    animated: false
  },
  methods: {
    drawCard: function() {
      this.animated = true;

      var self = this;
      self.animated = true;
      setTimeout(function() {
        self.animated = false;
      }, 1000);
      this.val = vals[Math.floor(Math.random() * vals.length)];
      var randSuit = suits[Math.floor(Math.random() * suits.length)];
      if (randSuit === "hearts" || randSuit === "diamonds") {
        this.color = "rgb(226,26,26)";
      } else {
        this.color = "black";
      }
      this.suit = randSuit;
    }
  },
  computed: {
    computedSuit: function() {
      return {
        spades: this.suit === "spades",
        clubs: this.suit === "clubs",
        diamonds: this.suit === "diamonds",
        hearts: this.suit === "hearts"
      };
    }
  }
});
